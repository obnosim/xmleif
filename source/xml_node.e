note
	description: "Common node of a XML file"
	author: "obnosim"

deferred class
	XML_NODE

inherit

	XML_CONSTANTS

	STRING_UTIL

	DEBUG_OUTPUT

feature {NONE} -- Initialization

	make_base
			-- Hook called by `make' before any other operation
		do
				-- To be redefined as needed
		end

	make (a_attributes: like attributes; a_parent: like parent)
			-- Initializes `Current' with `a_name' as a name,
			-- | `a_attributes' as `attributes'
			-- | and `a_parent' as `parent'
		do
			make_base
			attributes := a_attributes
			assign_attributes
			if attached a_parent then
				set_parent (a_parent)
			end
		ensure
			attributes_set: attributes = a_attributes
			parent_set: parent = a_parent
		end

	set_parent (a_parent: attached like parent)
			-- Assigns `a_parent' to `parent'
		deferred
		ensure
			set: parent = a_parent
		end

	assign_attributes
			-- Goes through `attributes' to pick them apart and set them into individual object attributes
			-- Called by `make'
		deferred
		end

feature {XML_FILE} -- Registration

	register (a_mapping_table: STRING_TABLE [XML_MAPPER [XML_NODE, detachable XML_NODE]])
			-- Adds a mapping procedure to `a_mapping_table' to build new instances of `Current'
		do
			a_mapping_table [tag] := create {XML_MAPPER [like current, like parent]}
		ensure
			instance_free: class
		end

feature {XML_FILE, XML_NODE} -- Construction

	add_child (a_child: XML_NODE)
			-- Makes `Current' know it contains `a_child'
		require
			already_the_parent: a_child.parent = current
		deferred
		end

	root: XML_NODE
			-- The root node of the tree containing `Current'
		do
			from
				result := current
			until
				not attached result.parent as la_parent
			loop
				result := la_parent
			end
		ensure
			root: not attached result.parent
		end

feature -- Access

	Tag: READABLE_STRING_8
			-- Name associated with this type of node
		deferred
		ensure
			instance_free: class
		end

	parent: detachable XML_NODE
			-- <Precursor>
		note
			option: stable
		deferred
		end

feature -- Identity report

	accepts_child (a_child: XML_NODE): BOOLEAN
			-- Whether `Current' accepts `a_child'
		deferred
		ensure
			instance_free: class
		end

feature -- Debug

	depth: INTEGER
			-- Depth of `Current' within the tree structure
		local
			l_parent: XML_NODE
		do
			from
				l_parent := parent
			until
				not attached l_parent
			loop
				result := result + 1
				l_parent := l_parent.parent
			end
		ensure
			root_is_zero: (result = 0) = not attached parent
		end

	indentation: STRING
			-- Indentation to use for `Current' when printing the whole tree structure
		local
			i, l_depth: like depth
		do
			from
				result := "  "
				i := 1
				l_depth := depth
			until
				i > l_depth
			loop
				result.append (once "  ")
				i := i + 1
			end
		end

feature -- Inline content

	accepts_content: BOOLEAN
			-- Whether `Current' accepts inline content
		deferred
		end

	put_content (a_content: STRING_8)
			-- Assigns `a_content' as the inline content of `Current'
			-- Might be called several times by the XML parser if e.g. the inline content is cut by inner nodes
		require
			supported: accepts_content
		deferred
		end

feature -- Attributes

	attributes: STRING_TABLE [READABLE_STRING_8]
			-- The xml attributes of `Current', accessible by name

	mandatory_int (a_key: like attributes.keys.item): INTEGER
			-- The integer attribute called `a_key'
			-- Its absence would render `Current' invalid
		do
			check
				present: attached facultative_int (a_key) as la_value
			then
				result := la_value.to_integer
			end
		end

	facultative_int (a_key: like attributes.keys.item): detachable INTEGER_REF
			-- The integer attribute called `a_key', if present
		do
			if attached attributes [a_key] as la_value then
				if la_value.is_integer then
					result := la_value.to_integer
				else
					format_mismatch ({INTEGER_32}, a_key, la_value)
				end
			end
		end

	mandatory_natural (a_key: like attributes.keys.item): NATURAL_32
			-- The integer attribute called `a_key'
			-- Its absence would render `Current' invalid
		do
			check
				present: attached facultative_natural (a_key) as la_value
			then
				result := la_value
			end
		end

	facultative_natural (a_key: like attributes.keys.item): detachable NATURAL_32_REF
			-- The integer attribute called `a_key', if present
		do
			if attached attributes [a_key] as la_value then
				if la_value.is_natural then
					result := la_value.to_natural
				else
					format_mismatch ({NATURAL_32}, a_key, la_value)
				end
			end
		end

	mandatory_real (a_key: like attributes.keys.item): REAL
			-- The real attribute called `a_key'
			-- Its absence would render `Current' invalid
		do
			check
				present: attached facultative_real (a_key) as la_value
			then
				result := la_value
			end
		end

	facultative_real (a_key: like attributes.keys.item): detachable REAL_32_REF
			-- The real attribute called `a_key', if present
		do
			if attached attributes [a_key] as la_value then
				if la_value.is_real then
					result := la_value.to_real
				else
					format_mismatch ({REAL_32}, a_key, la_value)
				end
			end
		end

	mandatory_string (a_key: like attributes.keys.item): attached READABLE_STRING_8
			-- The string attribute called `a_key'
			-- Its absence would render `Current' invalid
		do
			check
				present: attached attributes [a_key] as la_value
			then
				result := la_value
			end
		end

	facultative_string (a_key: like attributes.keys.item): detachable READABLE_STRING_8
			-- The string attribute called `a_key', if present
		do
			if attached attributes [a_key] as la_value then
				result := la_value
			end
		end

	mandatory_path (a_key: like attributes.keys.item): attached PATH
			-- The path attribute called `a_key'
			-- Its absence would render `Current' invalid
		do
			check
				present: attached attributes [a_key] as la_value
			then
				create result.make_from_string (la_value)
			end
		end

	facultative_path (a_key: like attributes.keys.item): detachable PATH
			-- The path attribute called `a_key', if present
		do
			if attached attributes [a_key] as la_value then
				create result.make_from_string (la_value)
			end
		end

	mandatory_boolean (a_key: like attributes.keys.item): BOOLEAN
			-- The boolean attribute called `a_key'
			-- Its absence would render `Current' invalid
		do
			check
				present: attached facultative_boolean (a_key) as la_value
			then
				result := la_value
			end
		end

	facultative_boolean (a_key: like attributes.keys.item): detachable BOOLEAN_REF
			-- The boolean attribute called `a_key', if present
		do
			if attached attributes [a_key] as la_value then
				if is_true (la_value) then
					result := True
				elseif is_false (la_value) then
					result := False
				else
					print ("%NUnrecognized boolean value: " + a_key)
				end
			end
		end

	opt_out_attribute (a_key: like attributes.keys.item): BOOLEAN
			-- The boolean attribute called `a_key', which defaults to `True' unless specifically set to `False' or `0'
		do
			result := attached attributes [a_key] as la_value implies not is_false (la_value)
		end

	opt_in_attribute (a_key: like attributes.keys.item): BOOLEAN
			-- The boolean attribute called `a_key', which defaults to `False' unless specifically set to `True' or `1'
		do
			if attached attributes [a_key] as la_value and then is_true (la_value) then
				result := true
			end
		end

feature -- Debug

	debug_output: STRING_8
			-- <Precursor>
		do
			result := "<"
			result.append (tag)
			result.append ("/>")
		end

feature {NONE} -- Debug

	format_mismatch (a_target_type: TYPE [ANY]; a_key, a_value: READABLE_STRING_GENERAL)
			-- Prints a warning about being unable to convert `a_value', associated to the attribute `a_key', to `a_target_type'
		do
			print ("%N" + tag + " could not convert the value of the attribute " + a_key + "=%"" + a_value + "%" to " + a_target_type.name)
		end

end
