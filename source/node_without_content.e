note
	description: "Trait of XML nodes that do not accept inline content"
	author: "obnosim"

deferred class
	NODE_WITHOUT_CONTENT

feature -- Inline content

	frozen accepts_content: BOOLEAN = False
			-- <Precursor>

	frozen put_content (a_content: STRING)
			-- <Precursor>
		do
			check
				not_allowed: false
			end
		end

end
