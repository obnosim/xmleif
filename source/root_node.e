note
	description: "Node that goes at the top of an XML tree"
	author: "obnosim"

deferred class
	ROOT_NODE

inherit

	XML_NODE

feature {NONE} -- Initialization

	set_parent (a_parent: attached like parent)
			-- <Precursor>
		do
			check
				not_applicable: false
			end
		end

feature {NONE} -- Implementation

	parent: detachable XML_NODE
			-- <Precursor>
		do
			do_nothing
		ensure then
			root: not attached result
		end

end
