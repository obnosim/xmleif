note
	description: "Summary description for {XML_CONSTANTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	XML_CONSTANTS

feature -- Constants

	Xml_declaration_open: IMMUTABLE_STRING_8 = "<?"
			-- Marks the beginning of the XML declaration tag

	Xml_declaration_close: IMMUTABLE_STRING_8 = "?>"
			-- Marks the end of the XML declaration tag

	Comment_open: IMMUTABLE_STRING_8 = "<!--"
			-- Marks the beginning of an XML comment tag

	Comment_close: IMMUTABLE_STRING_8 = "-->"
			-- Marks the end of an XML comment tag

	Bracket_open: CHARACTER = '<'
			-- Marks the beginning of a tag

	Bracket_close: CHARACTER = '>'
			-- Marks the end of a tag

	Slash: CHARACTER = '/'
		-- If immediately preceded by `Bracket_open', indicates a closing tag
		-- If immediately followed by `Bracket_close', indicates an empty tag
		-- Invalid in other contexts

	Dash: CHARACTER = '-'
			-- Part of XML comments

	Attribute_assignment: CHARACTER = '='
			-- Separates attribute keys from values

	Single_quote: CHARACTER = '%''
			-- Surrounds attributes in XML files

	Double_quote: CHARACTER = '%"'
			-- Surrounds attributes in XML files

feature {XML_FILE, XML_NODE} -- Cases

	Open_parent_tag: INTEGER_8 = 1
			-- Indicates that the current node might have children

	Empty_tag: like Open_parent_tag = 0
			-- Indicates that the current node cannot have children

	Close_parent_tag: like Open_parent_tag = -1
			-- Indicates that the current node is complete

end
