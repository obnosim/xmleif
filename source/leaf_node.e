note
	description: "XML node that accepts no inner node"
	author: "obnosim"

deferred class
	LEAF_NODE

inherit

	XML_NODE

feature {XML_NODE} -- Construction

	add_child (a_child: XML_NODE)
			-- <Precursor>
		do
			check
				not_applicable: false
			end
		end

	has_child (a_child: XML_NODE): BOOLEAN
			-- <Precursor>
		do
			result := false
		end

feature {XML_FILE} -- Construction

	accepts_child (a_child: XML_NODE): BOOLEAN
			-- <Precursor>
		do
			result := false
		ensure then
			terminal_node: not result
		end

end
