note
	description: "Virtual node created when parsing XML that cannot actually be declared in the source file."
	author: "obnosim"

deferred class
	SYNTHETIC_XML_NODE

inherit

	XML_NODE
		redefine
			register
		end

feature {NONE} -- Registration

	frozen register (a_mapping_table: STRING_TABLE [XML_MAPPER [XML_NODE, detachable XML_NODE]])
			-- <Precursor>
		do
			do_nothing
		ensure then
			no_effect: a_mapping_table ~ old a_mapping_table.twin
		end

end
