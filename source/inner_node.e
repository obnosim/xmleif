note
	description: "Node that goes within an XML tree at a position other than root"
	author: "obnosim"

deferred class
	INNER_NODE [PARENT_TYPE -> attached XML_NODE]

inherit

	XML_NODE
		redefine
			make
		end

feature {NONE} -- Initialization

	make (a_attributes: like attributes; a_parent: PARENT_TYPE)
			-- <Precursor>
		do
			parent := a_parent
			precursor (a_attributes, a_parent)
		end

feature {NONE} -- Initialization

	set_parent (a_parent: PARENT_TYPE)
			-- <Precursor>
		do
			parent := a_parent
			parent.add_child (current)
		end

feature -- Access

	parent: detachable PARENT_TYPE
			-- <Precursor>
		note
			option: stable
		attribute
		end

end
