note
	description: "[
		Provides static agents to instantiate generically-typed XML_NODEs
		Avoids having to write multiple versions of identical methods that operate on different implementation types
	]"
	author: "obnosim"

class
	XML_MAPPER [NODE -> XML_NODE create make end, PARENT -> detachable XML_NODE]

feature -- Instantiation

	spawn alias "()" (a_attributes: STRING_TABLE [READABLE_STRING_8]; a_parent: PARENT): XML_NODE
		do
			create {NODE} result.make (a_attributes, a_parent)
		end

end
