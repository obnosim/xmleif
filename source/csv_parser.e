note
	description: "Common routines for handling CSV data"
	author: "obnosim"

expanded class
	CSV_PARSER [D]

inherit

	ANY
		redefine
			default_create
		end

create
	default_create, make_with_separator

feature -- Initialization

	default_create
			-- `Current' shall use the default separator
		do
			make_with_separator (Default_separator)
		end

	make_with_separator (a_separator: like separator)
			-- `Current' shall use `a_separator' to split columns
		do
			separator := a_separator
		ensure
			separator = a_separator
		end

	with_comma: CSV_PARSER [D]
			-- A default parser, splitting on commas
		do
		ensure
			comma: result.separator = Default_separator
			instance_free: class
		end

feature -- Parsing

	separator: CHARACTER_8
			-- The value separator to use when parsing

	Default_separator: like separator = ','
			-- The default value separator in CSV files

	new_line: CHARACTER_8 = '%N'
			-- Separates lines in CSV files

	do_all (a_csv: READABLE_STRING_8; a_width, a_height: INTEGER; a_procedure: PROCEDURE [TUPLE [row, column: INTEGER; value: STRING]])
			-- Applies `a_procedure' to each item in `a_csv', passing row and column indexes (starting at 1) as well
			-- Cells in columns > `a_width' or in rows > `a_height' will be ignored
		local
			l_rows, l_cells: LIST [STRING]
			l_row, l_column: INTEGER
		do
			from
				l_row := 1
				l_rows := {STRING_8_UTIL}.split_and_trim (a_csv, new_line)
				l_rows.start
			until
				l_row > a_height or else l_rows.exhausted
			loop
				from
					l_column := 1
					l_cells := {STRING_8_UTIL}.split_and_trim (l_rows.item, separator)
					l_cells.start
				until
					l_cells.index > a_width or else l_cells.exhausted
				loop
					a_procedure (l_row, l_column, l_cells.item)
					l_column := l_column + 1
					l_cells.forth
				end
				l_row := l_row + 1
				l_rows.forth
			end
		end

	map_to_table (a_csv: READABLE_STRING_8; a_width, a_height: INTEGER; a_default: D; a_transformer: FUNCTION [TUPLE [value: STRING], D]): ARRAY2 [D]
			-- Maps `a_csv' into a grid of dimensions `a_width'x`a_height' by passing each value through `a_transformer'
			-- Cells for which there is no value in `a_csv' will hold `a_default'
			-- Cells in columns > `a_width' or in rows > `a_height' will be ignored
		require
			valid_dimensions: a_width > 0 and a_height > 0
		local
			l_rows, l_cells: LIST [STRING]
			l_row, l_column: INTEGER
		do
			create result.make_filled (a_default, a_height, a_width)
			from
				l_row := 1
				l_rows := {STRING_8_UTIL}.split_and_trim (a_csv, new_line)
				l_rows.start
			until
				l_row > a_height or else l_rows.exhausted
			loop
				from
					l_column := 1
					l_cells := {STRING_8_UTIL}.split_and_trim (l_rows.item, separator)
					l_cells.start
				until
					l_cells.index > a_width or else l_cells.exhausted
				loop
					result [l_row, l_column] := a_transformer (l_cells.item)
					l_column := l_column + 1
					l_cells.forth
				end
				l_row := l_row + 1
				l_rows.forth
			end
		ensure
			correct_size: result.width = a_width and result.height = a_height
		end

end
