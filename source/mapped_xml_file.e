note
	description: "XML_FILE implementation using a table to map tags to specific node types"
	author: "obnosim"

deferred class
	MAPPED_XML_FILE [ROOT -> XML_NODE]

inherit

	XML_FILE [ROOT]

feature {NONE} -- Mapping

	Mapping_table: STRING_TABLE [XML_MAPPER [XML_NODE, detachable XML_NODE]]
			-- Declares supported tags and the method used to build them
		deferred
		ensure
			instance_free: class
		end

	new_tag (a_name: like {XML_NODE}.tag; a_attributes: like {XML_NODE}.attributes; a_parent: detachable XML_NODE): XML_NODE
			-- <Precursor>
			-- For the time being, unsupported tags (as per `mapping_table') will cause failure
			-- Another solution could be to fall back to the precursor method
		do
			check attached mapping_table [a_name] as la_mapper then
				result := la_mapper (a_attributes, a_parent)
			end
		end

end
