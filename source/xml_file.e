note
	description: "Simple implementation of a read-only XML parser"
	author: "obnosim"

deferred class
	XML_FILE [ROOT -> XML_NODE]

inherit

	RAW_FILE
		rename
			new_line as write_new_line,
			empty as file_empty
		export
			{ANY} retrieved, Support_storable, readable, exists, close
		redefine
			retrieved
		end

	XML_CONSTANTS

feature {NONE} -- Chunk reading

	frozen index_in_chunk: like last_string.Lower
			-- Current index within `last_string'

	frozen chunks_exhausted: BOOLEAN
			-- Whether the last character of the last chunk has been read

	frozen forth_in_chunk
			-- Moves through the current chunk
			-- If it is over, read the next chunk
			-- If there is no other chunk to be read, set `chunks_exhausted' to `True'
		require
			has_chunk: attached last_string
			not_exhausted: not chunks_exhausted
		do
			index_in_chunk := index_in_chunk + 1
			if index_in_chunk > last_string.count then
				if readable then
					read_stream (4096)
					index_in_chunk := {like last_string}.Lower
				else
					chunks_exhausted := true
				end
			end
		end

	frozen item_in_chunk: like last_string.item
			-- The current character of the current chunk
		require
			has_chunk: attached last_string
			not_exhausted: not chunks_exhausted
		do
			result := last_string @ index_in_chunk
		end

feature {NONE} -- Parsing

	parse: detachable XML_NODE
			-- Reads through `Current', building a tree of typed nodes
		local
			l_mode: like Reading_tag_name
			l_char: like last_string.item
			l_node: XML_NODE
			l_buffer, l_tag_name, l_attribute_name: STRING_8
			l_attributes: like {XML_NODE}.attributes
		do
			from
				start
				create l_attribute_name.make_empty
				create l_tag_name.make_empty
				create last_string.make_filled ({STRING_8_UTIL}.Space, 4097)
				create l_buffer.make (last_string.capacity)
				create l_attributes.make (8)
				chunks_exhausted := false
				read_stream (4096)
				index_in_chunk := {like last_string}.Lower
				l_mode := Reading_xml_declaration
			invariant
				wiped_between_modes: l_buffer.is_empty
				no_premature_end: chunks_exhausted = (l_mode = Finished_reading)
			until
				l_mode = Finished_reading
			loop
				inspect l_mode
				when Reading_xml_declaration then
					from
					until
						not item_in_chunk.is_space
					loop
						forth_in_chunk
					end
					check item_in_chunk = '<' then
						forth_in_chunk
						check item_in_chunk = '?' then
							forth_in_chunk
							from
							until
								l_buffer.ends_with ("?>")
							loop
								l_buffer.extend (item_in_chunk)
								forth_in_chunk
							end
							l_mode := Reading_from_start
							l_buffer.wipe_out
						end
					end
				when Reading_from_start then
					from
					until
						l_mode /= Reading_from_start
					loop
						if chunks_exhausted then
							l_mode := Finished_reading
						else
							if item_in_chunk = '<' then
								l_mode := Reading_tag_name
							end
							forth_in_chunk
						end
					end
				when Reading_tag_name then
					from
					until
						l_mode /= Reading_tag_name
					loop
						l_char := item_in_chunk
						inspect l_char
						when ' ' then -- End of name
							check
								not_empty_name: not l_buffer.is_empty and not l_buffer.is_whitespace
							end
							l_tag_name := l_buffer.twin
							l_mode := Reading_attribute_name
							l_buffer.wipe_out
						when '/' then
							if l_buffer.is_empty then -- Actually the end of the enclosing tag
								check
									within_subnode: attached l_node
								end
								l_mode := Reading_closing_tag
								l_buffer.wipe_out
							else -- <tag/> End of leaf node without space after name
								check
									not_whitespace: not l_buffer.is_whitespace
								end
								l_tag_name := l_buffer.twin
								l_mode := Reading_leaf_node_end
								l_buffer.wipe_out
							end
						when '>' then -- <tag> End of opening tag without space after name
							check
								not_empty_name: not l_buffer.is_empty and not l_buffer.is_whitespace
							end
							l_tag_name := l_buffer.twin
							l_node := new_node (l_node, l_buffer, l_tag_name, l_attributes)
							l_mode := Reading_node_content
						when '!' then -- <!
							check
								only_as_beginning_of_comment: l_buffer.is_empty
							end
							l_buffer.extend (l_char)
						when '-' then -- <!- or <!--
							check
								only_within_comment: (l_buffer.count = 1 and then l_buffer.at (l_buffer.count) = '!') or (l_buffer.count = 2 and then l_buffer.at (l_buffer.count) = '-')
							end
							l_buffer.extend (l_char)
							if l_buffer.ends_with (Comment_open) then -- <!--
								l_mode := Reading_comment
								l_buffer.wipe_out
							end
						else
							check
								no_special_characters: l_char.is_alpha or l_char.is_digit
							end
							l_buffer.extend (l_char)
						end
						forth_in_chunk
					end
				when Reading_attribute_name then
					from
					until
						l_mode /= Reading_attribute_name
					loop
						l_char := item_in_chunk
						inspect l_char
						when '=' then -- Reached end of name
							check
								name_defined: not l_buffer.is_empty and not l_buffer.is_whitespace
							end
							l_attribute_name := l_buffer.twin
							l_mode := Reading_attribute_value
							l_buffer.wipe_out
						when '/' then -- End of tag <tag.../>
							check
								not_within_name: l_buffer.is_empty
								within_node: attached l_node
							end
							l_attribute_name := l_buffer.twin
							l_mode := Reading_leaf_node_end
						when '>' then -- Drop into the tag <tag...>
							check
								empty_name: l_buffer.is_empty
							end
							l_node := new_node (l_node, l_buffer, l_tag_name, l_attributes)
							l_mode := Reading_node_content
						else
							if l_char.is_space then
								check
									not_within_name: l_buffer.is_empty
								end
							else
								check
									no_special_characters: l_char.is_alpha or l_char.is_digit
								end
								l_buffer.extend (l_char)
							end
						end
						forth_in_chunk
					end
				when Reading_attribute_value then
					from
					until
						l_mode /= Reading_attribute_value
					loop
						l_char := item_in_chunk
						inspect l_char
						when '%'' then
							l_mode := Reading_single_quoted_attribute_value
						when '"' then
							l_mode := Reading_double_quoted_attribute_value
						end
						forth_in_chunk
					end
				when Reading_single_quoted_attribute_value then
					from
					until
						l_mode /= Reading_single_quoted_attribute_value
					loop
						l_char := item_in_chunk
						inspect l_char
						when '%'' then -- End of attribute value
							check
								no_duplicates: not l_attributes.has (l_attribute_name)
							end
							l_attributes [l_attribute_name] := l_buffer.twin
							l_mode := Reading_attribute_name
							l_buffer.wipe_out
						else
							l_buffer.extend (l_char)
						end
						forth_in_chunk
					end
				when Reading_double_quoted_attribute_value then
					from
					until
						l_mode /= Reading_double_quoted_attribute_value
					loop
						l_char := item_in_chunk
						inspect l_char
						when '"' then -- End of attribute value
							check
								no_duplicates: not l_attributes.has (l_attribute_name)
							end
							l_attributes [l_attribute_name] := l_buffer.twin
							l_mode := Reading_attribute_name
							l_buffer.wipe_out
						else
							l_buffer.extend (l_char)
						end
						forth_in_chunk
					end
				when Reading_leaf_node_end then
					from
					until
						l_mode /= Reading_leaf_node_end
					loop
						l_char := item_in_chunk
						inspect l_char
						when '>' then -- <tag.../> End of childless, contentless node
							result := new_node (l_node, l_buffer, l_tag_name, l_attributes)
								-- Back to parent node
							l_node := result.parent
							l_mode := if attached l_node then Reading_node_content else Reading_from_start end
						end
						forth_in_chunk
					end
				when Reading_node_end then
					from
						check
							within_node: attached l_node
						end
					until
						l_mode /= Reading_node_end
					loop
						l_char := item_in_chunk
						inspect l_char
						when '>' then -- <tag...> End of node with possibly children or content
							l_node := new_node (l_node, l_buffer, l_tag_name, l_attributes)
							l_mode := Reading_node_content
						end
						forth_in_chunk
					end
				when Reading_node_content then
					from
						check
							within_node: attached l_node
						end
					until
						l_mode /= Reading_node_content
					loop
						l_char := item_in_chunk
						inspect l_char
						when '<' then -- End of enclosing tag or beginning of enclosed tag
							if not l_buffer.is_empty and then not l_buffer.is_whitespace and then attached l_node then
								if l_node.accepts_content then
									l_node.put_content (l_buffer.twin)
								end
							end
							l_mode := Reading_tag_name
							l_buffer.wipe_out
						else
							l_buffer.extend (l_char)
						end
						forth_in_chunk
					end
				when Reading_closing_tag then
					from
						check
							within_node: attached l_node
						end
					until
						l_mode /= Reading_closing_tag
					loop
						l_char := item_in_chunk
						inspect l_char
						when '>' then -- End of closing tag
							check attached l_node and then l_buffer.same_string (l_node.tag) then
									-- Back to parent node
								result := l_node
								l_node := result.parent
								l_mode := if attached l_node then Reading_node_content else Reading_from_start end
								l_buffer.wipe_out
							end
						else
							check
								no_special_characters: l_char.is_alpha or l_char.is_digit
							end
							l_buffer.extend (l_char)
						end
						forth_in_chunk
					end
				when Reading_comment then
					from
					until
						l_mode /= Reading_comment
					loop
						l_char := item_in_chunk
						l_buffer.extend (l_char)
						if l_char = '>' then
							if l_buffer.ends_with (Comment_close) then -- -->
								l_mode := if attached l_node then Reading_node_content else Reading_from_start end
								l_buffer.wipe_out
							end
						end
						forth_in_chunk
					end
				end
			end
			check
				no_premature_end: attached result and then result = result.root
			end
		end

	Finished_reading: INTEGER_8 = unique
			-- Reached end of file

	Reading_xml_declaration: INTEGER_8 = unique
			-- Reading the common beginning of all xml files

	Reading_from_start: INTEGER_8 = unique
			-- Initial reading mode

	Reading_tag_name: INTEGER_8 = unique
			-- Reads the name of a tag
			-- <_...>

	Reading_leaf_node_end: INTEGER_8 = unique
			-- Reads the end of an empty tag
			-- <tag/_>

	Reading_node_end: INTEGER_8 = unique
			-- Reads the end of opening tags
			-- <tag_>

	Reading_closing_tag: INTEGER_8 = unique
			-- Reads a closing tag
			-- </???>

	Reading_attribute_name: INTEGER_8 = unique
			-- Reads the name of the next attribute on the current tag
			-- <tag ???=...

	Reading_attribute_value: INTEGER_8 = unique
			-- Reads the value of the current attribute
			-- <tag attr=?...?

	Reading_single_quoted_attribute_value: INTEGER_8 = unique
			-- Reads the value of the current attribute
			-- <tag attr='???'...

	Reading_double_quoted_attribute_value: INTEGER_8 = unique
			-- Reads the value of the current attribute
			-- <tag attr="???"...

	Reading_node_content: INTEGER_8 = unique
			-- Reads whatever is inside a node (can be other nodes or inline content or nothing)
			-- <tag>???</tag>

	Reading_comment: INTEGER_8 = unique
			-- Reads a comment
			-- <!--...-->

	frozen new_node (a_parent: detachable XML_NODE; a_buffer, a_name: STRING_8; a_attributes: like {XML_NODE}.attributes): XML_NODE
			-- A new node named `a_name', attached to `a_parent' and holding the attributes `a_attributes'
			-- Empties `a_buffer' and `a_attributes'
		require
			not_empty_name: not a_name.is_empty
			working_on_copy: a_name /= a_buffer
		local
			l_optimal_map: like {XML_NODE}.attributes
		do
			create l_optimal_map.make (a_attributes.count)
			l_optimal_map.merge (a_attributes)
			result := new_tag (a_name, l_optimal_map, a_parent)
			a_attributes.wipe_out
			a_buffer.wipe_out
		ensure
			coherent: result.parent = a_parent
			buffer_wiped: a_buffer.is_empty
			attributes_wiped: a_attributes.is_empty
		end

	new_tag (a_name: like {XML_NODE}.tag; a_attributes: like {XML_NODE}.attributes; a_parent: detachable XML_NODE): XML_NODE
			-- A new node named `a_name', attached to `a_parent'
		deferred
		end

feature -- Access

	retrieved: ROOT
			-- The entire tree represented by `Current'
		do
			check attached {ROOT} parse as la_root then
				result := la_root
			end
		end

end
