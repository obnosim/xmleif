﻿note
	description: "Known XML escaped characters and ways to handle them."
	author: "obnosim"

class
	XML_ESCAPING

feature {NONE} -- Implementation

	frozen Escaped_to_character: HASH_TABLE [CHARACTER_32, IMMUTABLE_STRING_32]
		once ("process")
			create result.make (255)
				-- XML reserved characters
			result.extend ('%"', "quot")
			result.extend ('&', "amp")
			result.extend ('%'', "apos")
			result.extend ('<', "lt")
			result.extend ('>', "gt")
				-- Symbols and special characters
			result.extend ('%/160/', "nbsp")
			result.extend ('¡', "iexcl")
			result.extend ('¢', "cent")
			result.extend ('£', "pound")
			result.extend ('¤', "curren")
			result.extend ('¥', "yen")
			result.extend ('¦', "brvbar")
			result.extend ('§', "sect")
			result.extend ('¨', "uml")
			result.extend ('©', "copy")
			result.extend ('ª', "ordf")
			result.extend ('«', "laquo")
			result.extend ('¬', "not")
			result.extend ('%/173/', "shy")
			result.extend ('®', "reg")
			result.extend ('¯', "macr")
			result.extend ('°', "deg")
			result.extend ('±', "plusmn")
			result.extend ('²', "sup2")
			result.extend ('³', "sup3")
			result.extend ('´', "acute")
			result.extend ('µ', "micro")
			result.extend ('¶', "para")
			result.extend ('·', "middot")
			result.extend ('¸', "cedil")
			result.extend ('¹', "sup1")
			result.extend ('º', "ordm")
			result.extend ('»', "raquo")
			result.extend ('¼', "frac14")
			result.extend ('½', "frac12")
			result.extend ('¾', "frac34")
			result.extend ('¿', "iquest")
				-- Extended latin characters
			result.extend ('À', "Agrave")
			result.extend ('Á', "Aacute")
			result.extend ('Â', "Acirc")
			result.extend ('Ã', "Atilde")
			result.extend ('Ä', "Auml")
			result.extend ('Å', "Aring")
			result.extend ('Æ', "AElig")
			result.extend ('Ç', "Ccedil")
			result.extend ('È', "Egrave")
			result.extend ('É', "Eacute")
			result.extend ('Ê', "Ecirc")
			result.extend ('Ë', "Euml")
			result.extend ('Ì', "Igrave")
			result.extend ('Í', "Iacute")
			result.extend ('Î', "Icirc")
			result.extend ('Ï', "Iuml")
			result.extend ('Ð', "ETH")
			result.extend ('Ñ', "Ntilde")
			result.extend ('Ò', "Ograve")
			result.extend ('Ó', "Oacute")
			result.extend ('Ô', "Ocirc")
			result.extend ('Õ', "Otilde")
			result.extend ('Ö', "Ouml")
			result.extend ('×', "times")
			result.extend ('Ø', "Oslash")
			result.extend ('Ù', "Ugrave")
			result.extend ('Ú', "Uacute")
			result.extend ('Û', "Ucirc")
			result.extend ('Ü', "Uuml")
			result.extend ('Ý', "Yacute")
			result.extend ('Þ', "THORN")
			result.extend ('ß', "szlig")
			result.extend ('à', "agrave")
			result.extend ('á', "aacute")
			result.extend ('â', "acirc")
			result.extend ('ã', "atilde")
			result.extend ('ä', "auml")
			result.extend ('å', "aring")
			result.extend ('æ', "aelig")
			result.extend ('ç', "ccedil")
			result.extend ('è', "egrave")
			result.extend ('é', "eacute")
			result.extend ('ê', "ecirc")
			result.extend ('ë', "euml")
			result.extend ('ì', "igrave")
			result.extend ('í', "iacute")
			result.extend ('î', "icirc")
			result.extend ('ï', "iuml")
			result.extend ('ð', "eth")
			result.extend ('ñ', "ntilde")
			result.extend ('ò', "ograve")
			result.extend ('ó', "oacute")
			result.extend ('ô', "ocirc")
			result.extend ('õ', "otilde")
			result.extend ('ö', "ouml")
			result.extend ('÷', "divide")
			result.extend ('ø', "oslash")
			result.extend ('ù', "ugrave")
			result.extend ('ú', "uacute")
			result.extend ('û', "ucirc")
			result.extend ('ü', "uuml")
			result.extend ('ý', "yacute")
			result.extend ('þ', "thorn")
			result.extend ('ÿ', "yuml")
			result.extend ('Œ', "OElig")
			result.extend ('œ', "oelig")
			result.extend ('Š', "Scaron")
			result.extend ('š', "scaron")
			result.extend ('Ÿ', "Yuml")
			result.extend ('ƒ', "fnof")
			result.extend ('ˆ', "circ")
			result.extend ('˜', "tilde")
				-- Greek characters
			result.extend ('Α', "Alpha")
			result.extend ('Β', "Beta")
			result.extend ('Γ', "Gamma")
			result.extend ('Δ', "Delta")
			result.extend ('Ε', "Epsilon")
			result.extend ('Ζ', "Zeta")
			result.extend ('Η', "Eta")
			result.extend ('Θ', "Theta")
			result.extend ('Ι', "Iota")
			result.extend ('Κ', "Kappa")
			result.extend ('Λ', "Lambda")
			result.extend ('Μ', "Mu")
			result.extend ('Ν', "Nu")
			result.extend ('Ξ', "Xi")
			result.extend ('Ο', "Omicron")
			result.extend ('Π', "Pi")
			result.extend ('Ρ', "Rho")
			result.extend ('Σ', "Sigma")
			result.extend ('Τ', "Tau")
			result.extend ('Υ', "Upsilon")
			result.extend ('Φ', "Phi")
			result.extend ('Χ', "Chi")
			result.extend ('Ψ', "Psi")
			result.extend ('Ω', "Omega")
			result.extend ('α', "alpha")
			result.extend ('β', "beta")
			result.extend ('γ', "gamma")
			result.extend ('δ', "delta")
			result.extend ('ε', "epsilon")
			result.extend ('ζ', "zeta")
			result.extend ('η', "eta")
			result.extend ('θ', "theta")
			result.extend ('ι', "iota")
			result.extend ('κ', "kappa")
			result.extend ('λ', "lambda")
			result.extend ('μ', "mu")
			result.extend ('ν', "nu")
			result.extend ('ξ', "xi")
			result.extend ('ο', "omicron")
			result.extend ('π', "pi")
			result.extend ('ρ', "rho")
			result.extend ('ς', "sigmaf")
			result.extend ('σ', "sigma")
			result.extend ('τ', "tau")
			result.extend ('υ', "upsilon")
			result.extend ('φ', "phi")
			result.extend ('χ', "chi")
			result.extend ('ψ', "psi")
			result.extend ('ω', "omega")
			result.extend ('ϑ', "thetasym")
			result.extend ('ϒ', "upsih")
			result.extend ('ϖ', "piv")
				-- Typographic symbols
			result.extend ('%/8194/', "ensp")
			result.extend ('%/8195/', "emsp")
			result.extend ('%/8201/', "thinsp")
			result.extend ('%/8204/', "zwnj")
			result.extend ('%/8205/', "zwj")
			result.extend ('%/8206/', "lrm")
			result.extend ('%/8207/', "rlm")
			result.extend ('–', "ndash")
			result.extend ('—', "mdash")
			result.extend ('‘', "lsquo")
			result.extend ('’', "rsquo")
			result.extend ('‚', "sbquo")
			result.extend ('“', "ldquo")
			result.extend ('”', "rdquo")
			result.extend ('„', "bdquo")
			result.extend ('†', "dagger")
			result.extend ('‡', "Dagger")
			result.extend ('•', "bull")
			result.extend ('…', "hellip")
			result.extend ('‰', "permil")
			result.extend ('′', "prime")
			result.extend ('″', "Prime")
			result.extend ('‹', "lsaquo")
			result.extend ('›', "rsaquo")
				-- Other symbols
			result.extend ('‾', "oline")
			result.extend ('⁄', "frasl")
			result.extend ('€', "euro")
			result.extend ('ℑ', "image")
			result.extend ('℘', "weierp")
			result.extend ('ℜ', "real")
			result.extend ('™', "trade")
			result.extend ('ℵ', "alefsym")
			result.extend ('←', "larr")
			result.extend ('↑', "uarr")
			result.extend ('→', "rarr")
			result.extend ('↓', "darr")
			result.extend ('↔', "harr")
			result.extend ('↵', "crarr")
			result.extend ('⇐', "lArr")
			result.extend ('⇑', "uArr")
			result.extend ('⇒', "rArr")
			result.extend ('⇓', "dArr")
			result.extend ('⇔', "hArr")
			result.extend ('∀', "forall")
			result.extend ('∂', "part")
			result.extend ('∃', "exist")
			result.extend ('∅', "empty")
			result.extend ('∇', "nabla")
			result.extend ('∈', "isin")
			result.extend ('∉', "notin")
			result.extend ('∋', "ni")
			result.extend ('∏', "prod")
			result.extend ('∑', "sum")
			result.extend ('−', "minus")
			result.extend ('∗', "lowast")
			result.extend ('√', "radic")
			result.extend ('∝', "prop")
			result.extend ('∞', "infin")
			result.extend ('∠', "ang")
			result.extend ('∧', "and")
			result.extend ('∨', "or")
			result.extend ('∩', "cap")
			result.extend ('∪', "cup")
			result.extend ('∫', "int")
			result.extend ('∴', "there4")
			result.extend ('∼', "sim")
			result.extend ('≅', "cong")
			result.extend ('≈', "asymp")
			result.extend ('≠', "ne")
			result.extend ('≡', "equiv")
			result.extend ('≤', "le")
			result.extend ('≥', "ge")
			result.extend ('⊂', "sub")
			result.extend ('⊃', "sup")
			result.extend ('⊄', "nsub")
			result.extend ('⊆', "sube")
			result.extend ('⊇', "supe")
			result.extend ('⊕', "oplus")
			result.extend ('⊗', "otimes")
			result.extend ('⊥', "perp")
			result.extend ('⋅', "sdot")
			result.extend ('⋮', "vellip")
			result.extend ('⌈', "lceil")
			result.extend ('⌉', "rceil")
			result.extend ('⌊', "lfloor")
			result.extend ('⌋', "rfloor")
			result.extend ('〈', "lang")
			result.extend ('〉', "rang")
			result.extend ('◊', "loz")
			result.extend ('♠', "spades")
			result.extend ('♣', "clubs")
			result.extend ('♥', "hearts")
			result.extend ('♦', "diams")
		ensure
			instance_free: class
		end

	frozen Character_to_escaped: HASH_TABLE [IMMUTABLE_STRING_32, CHARACTER_32]
			-- Converse of `Escaped_to_character'
		once ("process")
			create result.make (Escaped_to_character.count)
			across
				Escaped_to_character as e_to_c
			loop
				result [e_to_c.item] := e_to_c.key
			end
		ensure
			instance_free: class
		end

feature -- Exact encode/decode

	frozen is_encoded_named_entity (a_value: READABLE_STRING_32): BOOLEAN
			-- Whether `a_value' is a known encoded entity name
		require
			name_only: not a_value.starts_with ("&") and not a_value.ends_with (";")
		do
			result := Escaped_to_character.has (a_value)
		ensure
			instance_free: class
		end

	frozen decode_named_entity (a_value: READABLE_STRING_32): CHARACTER_32
			-- The original character `a_value' represents if any, or else the null character
			-- ex. "amp" -> '&'
		require
			name_only: not a_value.starts_with ("&") and not a_value.ends_with (";")
		do
			result := Escaped_to_character [a_value]
		ensure
			null_if_not_escaped: (result = result.default) = not is_encoded_named_entity (a_value)
			instance_free: class
		end

	frozen is_encodable_as_named_entity (a_value: CHARACTER_32): BOOLEAN
			-- Whether `a_value' has a matching entity name
		do
			result := Character_to_escaped.has (a_value)
		ensure
			instance_free: class
		end

	frozen encoded_named_entity (a_value: CHARACTER_32): detachable STRING_32
			-- The escaped entity form of `a_value', if any, without surrounding characters
			-- ex. '&' -> "amp"
		do
			if attached Character_to_escaped [a_value] as la_escape then
				result := la_escape
			end
		ensure
			void_if_inescapable: is_encodable_as_named_entity (a_value) = attached result
			instance_free: class
		end

	frozen encode_to_decimal (a_value: CHARACTER_32): STRING_8
			-- The escaped decimal token of `a_value'
		do
			result := "#" + a_value.code.out
		ensure
			with_hash: result [result.Lower] = '#'
			reversible: decode_numeric_value (result) = a_value
			instance_free: class
		end

	frozen decode_numeric_value (a_value: READABLE_STRING_32): CHARACTER_32
			-- The character whose code is indicated by `a_value', whether as a decimal or as an hexadecimal.
		require
			not_empty: a_value.count > 1
			code_only: a_value [a_value.Lower] = '#' and not a_value.has (';')
			valid_format: a_value [a_value.Lower + 1] /= 'x' implies across (a_value.Lower + 1) |..| a_value.count is i all a_value [i].is_digit end
			valid_format: a_value [a_value.Lower + 1] = 'x' implies across (a_value.Lower + 2) |..| a_value.count is i all a_value [i].is_hexa_digit end
		do
			if not a_value.is_empty then
				if a_value [a_value.Lower + 1] = 'x' then
					result := {HEX_UTIL}.hex_string_to_decimal (a_value.substring (a_value.Lower + 2, a_value.count).to_string_8).to_character_32
				else
					result := a_value.substring (a_value.Lower + 1, a_value.count).to_natural_32.to_character_32
				end
			end
		ensure
			instance_free: class
		end

feature -- Flexible encode/decode

	frozen encode_utf8 (a_value: CHARACTER_32): READABLE_STRING_32
			-- If necessary, the escape token for `a_value', without surrounding characters
		do
			if a_value.is_character_8 then
				create {STRING_32} result.make_filled (a_value.to_character_8, 1)
			elseif attached encoded_named_entity (a_value) as la_name then
				result := la_name
			else
				result := encode_to_decimal (a_value)
			end
		ensure
			reversible: decode_encoded_character (result) = a_value or else result [result.Lower] = a_value
			instance_free: class
		end

	frozen decode_encoded_character (a_token: READABLE_STRING_32): CHARACTER_32
			-- The original value `a_token' represents
		require
			inner_only: not a_token.has ('&') and not a_token.has (';')
		do
			if not a_token.is_empty then
				if a_token [a_token.Lower] = '#' then
					result := decode_numeric_value (a_token)
				else
					result := decode_named_entity (a_token)
				end
			end
		ensure
			instance_free: class
		end

feature -- Strings

	decode (a_source: STRING_32)
			-- Replaces all escape tokens in `a_source' with the relevant characters
		local
			i, j: like {STRING_32}.Lower
			l_original_char: CHARACTER_32
			l_token, l_original: like encode_utf8.to_string_32
		do
			from
				i := a_source.index_of ('&', a_source.Lower)
			until
				i = 0
			loop
				j := a_source.index_of (';', i)
				if j > i then
					l_token := a_source.substring (i + 1, j - 1).to_string_32
					l_original_char := decode_encoded_character (l_token)
					if l_original_char /= l_original_char.default then
						create l_original.make_filled (l_original_char, 1)
						a_source.replace_substring (l_original, i, j)
					end
				end
				i := a_source.index_of ('&', i + 1)
			end
		ensure
			equivalent_algorithms: a_source.same_string (decoded (old (a_source + "")))
			instance_free: class
		end

	decoded (a_source: READABLE_STRING_32): STRING_32
			-- A copy of `a_source' where all escape tokens were replaced with the relevant characters
		local
			i, j: like {STRING_32}.Lower
		do
			from
				create result.make (a_source.count)
				i := a_source.index_of ('&', a_source.Lower)
				if i = 0 then
					result := a_source.as_string_32.twin
				end
			until
				i = 0
			loop
				if i > j then
					result.append_substring (a_source, j + 1, i - 1)
				end
				j := a_source.index_of (';', i)
				check
					valid_xml_escape_pattern: j > i
				then
					result.extend (decode_encoded_character (a_source.substring (i + 1, j - 1).to_string_32))
				end
				i := a_source.index_of ('&', i + 1)
			end
		ensure
			no_side_effects: a_source.same_string (old (a_source + "")) and result /= a_source
			instance_free: class
		end

end
