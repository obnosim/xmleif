note
	description: "Useful constants for handling strings"
	author: "obnosim"

class
	STRING_UTIL

feature -- Cleanup

	trim (a_list: LIST [STRING_GENERAL])
			-- Trims whitespace from all items of `a_list'
			-- Removes empty items
		do
			from
				a_list.start
			until
				a_list.exhausted
			loop
				a_list.item.adjust
				if a_list.item.is_whitespace or a_list.item.is_empty then
					a_list.remove
				else
					a_list.forth
				end
			end
		ensure
			instance_free: class
		end

feature -- Status report

	string_is_empty (a_string: detachable READABLE_STRING_GENERAL): BOOLEAN
			-- Whether `a_string' is considered empty
			-- Are considered empty:
			-- - `void'
			-- - the empty string
			-- - strings containing only whitespace
		do
			result := not attached a_string as la_string or else la_string.is_empty or else la_string.is_whitespace
		ensure
			instance_free: class
		end

	is_true (a_string: READABLE_STRING_GENERAL): BOOLEAN
			-- Whether `a_string' can be understood as `True'
			-- Are considered true:
			-- - 'true' (case insensitive)
			-- - 'True_int_string'
		do
			result := a_string.is_case_insensitive_equal ({STRING_8_UTIL}.True_string) or a_string.same_string ({STRING_8_UTIL}.True_int_string)
		ensure
			instance_free: class
		end

	is_false (a_string: READABLE_STRING_GENERAL): BOOLEAN
			-- Whether `a_string' can be understood as `False'
			-- Are considered false:
			-- - 'false' (case insensitive)
			-- - `False_int_string'
		do
			result := a_string.is_case_insensitive_equal ({STRING_8_UTIL}.False_string) or a_string.same_string ({STRING_8_UTIL}.False_int_string)
		ensure
			instance_free: class
		end

feature {NONE} -- Debug

	token_not_found (a_source, a_token: READABLE_STRING_GENERAL)
			-- Logs an error saying `a_token' was not found in `a_source'
		do
			print ("Token '" + a_token + "' not found in string '" + a_source + "'")
		ensure
			instance_free: class
		end

end
