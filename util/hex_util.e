note
	description: "Provides utility methods to handle hexadecimal strings"
	author: "obnosim"

expanded class
	HEX_UTIL

feature -- Constants

	Base: NATURAL_8 = 16
			-- Base (digit count) of the hexadecimal numeric system

feature -- Conversion

	hex_string_to_decimal (a_string: READABLE_STRING_8): INTEGER
			-- `a_string', understood as an hexadecimal number
		require
			valid_length: 0 < a_string.count and a_string.count <= 10
			valid_string: across a_string as i all i.item.is_hexa_digit or else (i.item = '-' and then i.cursor_index = a_string.Lower) end
			no_overflow: Base ^ a_string.count - 1 < {INTEGER}.Max_value
		local
			l_position: INTEGER
		do
			from
				l_position := a_string.count
			until
				l_position < a_string.Lower + 1
			loop
				result := result + (hex_char_to_decimal (a_string [l_position]) * Base ^ (l_position - 1)).floor
				l_position := l_position - 1
			variant
				l_position
			end
			if l_position = a_string.Lower then
				if a_string [l_position] = '-' then
					result := - result
				else
					result := result + (hex_char_to_decimal (a_string [l_position]) * Base ^ (l_position - 1)).floor
				end
			end
		ensure
			sign_respected: result <= 0 implies a_string [a_string.lower] = '-'
			instance_free: class
		end

	hex_char_to_decimal (a_char: CHARACTER_8): NATURAL_8
			-- The value of `a_char' as an hexadecimal digit
		require
			hex: a_char.is_hexa_digit
		do
			inspect a_char
			when '0' then
				result := 0x0
			when '1' then
				result := 0x1
			when '2' then
				result := 0x2
			when '3' then
				result := 0x3
			when '4' then
				result := 0x4
			when '5' then
				result := 0x5
			when '6' then
				result := 0x6
			when '7' then
				result := 0x7
			when '8' then
				result := 0x8
			when '9' then
				result := 0x9
			when 'A', 'a' then
				result := 0xA
			when 'B', 'b' then
				result := 0xB
			when 'C', 'c' then
				result := 0xC
			when 'D', 'd' then
				result := 0xD
			when 'E', 'e' then
				result := 0xE
			when 'F', 'f' then
				result := 0xF
			end
		ensure
			instance_free: class
			valid: 0 <= result and result < Base
			reversible: natural_to_hex (result) = a_char
		end

	frozen natural_to_hex (a_int: NATURAL): CHARACTER_8
		require
			within_range: a_int < Base
		do
			result := a_int.to_hex_character
		ensure
			instance_free: class
			reversible: hex_char_to_decimal (result) = a_int
		end

	frozen integer_to_hex (a_int: INTEGER): CHARACTER_8
		require
			within_range: 0 <= a_int and a_int < Base
		do
			result := a_int.to_hex_character
		ensure
			instance_free: class
			reversible: hex_char_to_decimal (result) = a_int
		end

end
