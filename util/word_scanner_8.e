note
	description: "Iterates through given strings, looking for words separated by a given character"
	author: "obnosim"

class
	WORD_SCANNER_8

inherit

	ITERATION_CURSOR [STRING_8]

	ITERABLE [STRING_8]

create
	make, make_whitespace, make_any

feature -- Initialization

	make (a_target: like target; a_separator: like target.item)
			-- `Current' shall iterate over words in `a_target' delimited by any of `a_separators'
		do
			make_any (a_target, <<a_separator>>)
		end

	make_whitespace (a_target: like target)
		do
			make_any (a_target, " %T%N")
		end

	make_any (a_target: like target; a_separators: READABLE_INDEXABLE [like target.item])
			-- `Current' shall iterate over words in `a_target' delimited by any of `a_separators'
		local
			i: like separators.lower
		do
			target := a_target
			from
				create separators.make_filled (({like separators.item}).default, a_separators.lower, a_separators.upper)
				i := separators.lower
			until
				i > separators.upper
			loop
				separators [i] := a_separators [i]
				i := i + 1
			end
			start
		ensure then
			separators.count = (1 + a_separators.upper - a_separators.lower) and then across a_separators is s all separators.has (s) end
		end

feature {NONE} -- Implementation

	target: READABLE_STRING_8
			-- Data over which `Current' iterates

	internal_item: detachable like item
			-- `item' if not `after'

	separators: ARRAY [like target.item]
			-- Separates words

	start_index: INTEGER
			-- Current index within `target' of the beginning of the current item

	target_exhausted: BOOLEAN
			-- Whether the end of `target' has been reached

	skip_separators
			-- Jumps `start_index' over any number of consecutive occurrences of `separator'
		do
			from
			until
				target_exhausted or else not separators.has (target [start_index])
			loop
				start_index := start_index + 1
				if start_index > target.count then
					target_exhausted := true
				end
			end
		ensure
			aftet_or_valid: target_exhausted xor (target.valid_index (start_index) and then not separators.has (target [start_index]))
		end

feature -- Access

	new_cursor: like Current
			-- <Precursor>
		do
			result := current
		end

	start
			-- Moves back to the beginning
		do
			internal_item := void
			if target.is_empty then
				after := true
				target_exhausted := true
			else
				after := false
				target_exhausted := false
				start_index := target.Lower
				skip_separators
				forth
			end
		ensure
			after_or_valid: target_exhausted or target.valid_index (start_index)
		end

	forth
			-- <Precursor>
		local
			l_done: BOOLEAN
			l_end_index: like start_index
		do
			if target_exhausted then
				after := true
			else
				from
					l_end_index := start_index + 1
				until
					l_done
				loop
					if l_end_index > target.count then
							-- Reached end of string
						internal_item := target.substring (start_index, l_end_index - 1).to_string_8
						target_exhausted := true
						l_done := true
					elseif separators.has (target [l_end_index]) then
							-- Found end of word
						internal_item := target.substring (start_index, l_end_index - 1).to_string_8
						start_index := l_end_index
						skip_separators
						l_done := true
					end
					l_end_index := l_end_index + 1
				end
			end
		ensure then
			clean: attached internal_item as la_item implies not across separators is s some la_item.has (s) end
		end

	item: STRING_8
			-- The last word found by `forth', if not `after'
		do
			check attached internal_item as la_item then
				result := la_item
			end
		end

	after: BOOLEAN
			-- <Precursor>

	collected: LINKED_LIST [like item]
		do
			from
				create result.make
				start
			until
				after
			loop
				result.extend (item)
				forth
			end
		end

	split_on_whitespace (a_string: like target): like collected
		do
			result := (create {WORD_SCANNER_8}.make_whitespace (a_string)).collected
		ensure
			instance_free: class
		end

end
