note
	description: "Useful constants for handling strings"
	author: "obnosim"

expanded class
	STRING_32_UTIL

inherit

	STRING_UTIL

feature -- Constants

	Empty: IMMUTABLE_STRING_32
			-- A static instance of an immutable empty string
		once ("process")
			create result.make_empty
		ensure
			empty: result.is_empty
			instance_free: class
		end

feature -- Once constants

	Filesystem_separator: IMMUTABLE_STRING_32
			-- Convenience access to the system-defined directory separator
		once ("process")
			create result.make_filled (operating_environment.Directory_separator, 1)
		ensure
			instance_free: class
			coherent: not result.is_empty
		end

feature -- Cleanup

	split_and_trim (a_string: STRING_32; a_separator: CHARACTER_32): LIST [attached STRING_32]
			-- Splits `a_string' around `a_separator' and trims all parts
		do
			result := a_string.split (a_separator)
			trim (result)
		ensure
			instance_free: class
		end

	words (a_string: READABLE_STRING_32; a_start_index, a_end_index: INTEGER): ARRAYED_LIST [attached STRING_32]
			-- Words in `a_string', separated by any whitespace character, found exclusively between `a_start_index' and `a_end_index'
		require
			valid_bounds: a_string.Lower <= a_start_index and a_end_index <= a_string.count
			coherent_bounds: a_start_index <= a_end_index
		local
			i, l_word_start: INTEGER
			l_in_word: BOOLEAN
		do
			from
				create result.make (16)
				i := a_start_index
			until
				i > a_end_index
			loop
				if a_string [i].is_space then
					if l_in_word then
							-- End of word
						result.extend (a_string.substring (l_word_start, i - 1))
						l_in_word := false
					end
				else
					if not l_in_word then
						l_word_start := i
						l_in_word := true
					end
				end
				i := i + 1
			end
			if l_in_word then
					-- Last word without space after it
				result.extend (a_string.substring (l_word_start, a_end_index))
			end
		ensure
			instance_free: class
		end

	split_substring (a_string: READABLE_STRING_32; a_separator: CHARACTER_32; a_start_index, a_end_index: INTEGER): ARRAYED_LIST [attached STRING_32]
			-- Like `{READABLE_STRING_32}.split(a_separator)', but applied to only the characters between `a_start_index' and `a_end_index' of `a_string'
		require
			valid_bounds: a_string.Lower <= a_start_index and a_end_index <= a_string.count
			coherent_bounds: a_start_index <= a_end_index
		local
			i, j: like {READABLE_STRING_32}.Lower
		do
			from
				create result.make (16)
				i := a_start_index
				j := i
			until
				i > a_end_index
			loop
				if a_string [i] = a_separator and then i > j then
					result.extend (a_string.substring (j, i))
					j := i
				end
				i := i + 1
			end
		ensure
			instance_free: class
		end

	merge_lines (a_base: STRING_32; a_part: READABLE_STRING_32; a_separator: CHARACTER_32)
			-- Adds `a_part', stripped of external white space, to the end of `a_base', separated by `a_separator'.
			-- Useful to merge back a paragraph that was split into several lines while ignoring tabulations and such.
		local
			l_start, l_end: like {STRING_32}.lower
		do
			from
				l_start := a_part.lower
			until
				l_start > a_part.count or else not a_part [l_start].is_space
			loop
				l_start := l_start + 1
			end
			if l_start <= a_part.count then
				a_base.extend (a_separator)
				from
					l_end := a_part.count
				until
					l_end <= l_start or else not a_part [l_end].is_space
				loop
					l_end := l_end - 1
				end
				a_base.append (a_part.substring (l_start, l_end))
			end
		ensure
			instance_free: class
		end

	trimmed (a_raw: READABLE_STRING_32): STRING_32
			-- A version of `a_raw' without any surrounding white space.
			-- The result might be `a_raw' if it is already clean.
		local
			l_start, l_end: like {STRING_32}.lower
		do
			from
				l_start := a_raw.lower
			until
				l_start > a_raw.count or else not is_whitespace (a_raw [l_start])
			loop
				l_start := l_start + 1
			end
			if l_start > a_raw.count then
				result := empty
			else
				from
					l_end := a_raw.count
				until
					l_end <= l_start or else not is_whitespace (a_raw [l_start])
				loop
					l_end := l_end - 1
				end
				if l_start = a_raw.lower and l_end = a_raw.count then
					result := a_raw
				else
					result := a_raw.substring (l_start, l_end)
				end
			end
		ensure
			no_side_effect: a_raw.same_string (old a_raw.string)
			cleaned: (not result.is_empty) implies (not is_whitespace (result [result.lower]) and not is_whitespace (result [result.count]))
			instance_free: class
		end

	trimmed_copy (a_raw: READABLE_STRING_32): STRING_32
			-- A version of `a_raw' without any surrounding white space.
			-- Unlike `trimmed', the result will always be a new object.
		do
			result := trimmed (a_raw)
			if result = a_raw then
				result := result.string
			end
		ensure
			never_input: result /= a_raw
			no_side_effect: a_raw.same_string (old a_raw.string)
			cleaned: (not result.is_empty) implies (not is_whitespace (result [result.lower]) and not is_whitespace (result [result.count]))
			instance_free: class
		end

	is_whitespace (a_char: CHARACTER_32): BOOLEAN
			-- Whether `a_char' is considered white space
		do
			result := a_char.is_space or a_char.is_control
		ensure
			instance_free: class
		end

	reduced_whitespace (a_source: READABLE_STRING_32): STRING_32
		do
			across
				words (a_source, a_source.lower, a_source.count) as i
			from
				create result.make (a_source.count)
				if not i.after then
					result.append (i.item)
					i.forth
				end
			loop
				result.extend (' ')
				result.append (i.item)
			end
		ensure
			instance_free: class
		end

feature -- Processing

	split (a_string: STRING_32; a_separator: CHARACTER_32): LINKED_LIST [STRING_32]
			-- `a_string', split around `a_separator'
		do
			result := (create {WORD_SCANNER_32}.make (a_string, a_separator)).collected
		ensure
			instance_free: class
		end

	replace_all (a_string: STRING_32; a_source, a_target: CHARACTER_32)
			-- Replaces all occurrences of `a_source' within `a_string' with `a_target'
		local
			i: like {STRING_32}.Lower
		do
			from
				i := a_string.Lower
			until
				i > a_string.count
			loop
				if a_string [i] = a_source then
					a_string [i] := a_target
				end
				i := i + 1
			end
		ensure
			instance_free: class
		end

	substitute (a_string: READABLE_STRING_32; a_value: READABLE_STRING_32): STRING_32
			-- Replaces the first occurrence of `a_value' and `a_values'
		local
			l_key: STRING_32
			i: INTEGER
		do
			create result.make_from_string (a_string)
			l_key := "{1}"
			i := result.substring_index (l_key, result.Lower)
			if i > 0 then
				result.replace_substring (a_value, i, i + l_key.count - 1)
			else
				debug ("strings")
					token_not_found (a_string, l_key)
				end
			end
		ensure
			instance_free: class
		end

	substitute_many (a_string: READABLE_STRING_32; a_values: READABLE_INDEXABLE [READABLE_STRING_32]): STRING_32
			-- Replaces the first occurrence of `a_value' and `a_values'
		local
			l_key: STRING_32
			i: INTEGER
		do
			create result.make_from_string (a_string)
			across
				a_values as i_values
			loop
				l_key := "{"
				l_key.append_integer (i_values.cursor_index)
				l_key.extend ('}')
				i := result.substring_index (i_values.item, result.Lower)
				if i > 0 then
					result.replace_substring (i_values.item, i, i + l_key.count - 1)
				else
					debug ("strings")
						token_not_found (a_string, l_key)
					end
				end
			end
		ensure
			instance_free: class
		end

feature -- Status report

	string_starts_with (a_string: detachable READABLE_STRING_32; a_character: CHARACTER_32): BOOLEAN
			-- Whether `a_string' begins with `a_character'
			-- Similar to `a_string.starts_with' but with a single character
		do
			if attached a_string then
				result := not a_string.is_empty and then a_string.item (a_string.Lower) = a_character
			end
		ensure
			instance_free: class
		end

	string_ends_with (a_string: detachable READABLE_STRING_32; a_character: CHARACTER_32): BOOLEAN
			-- Whether `a_string' ends with `a_character'
			-- Similar to `a_string.ends_with' but with a single character
		do
			if attached a_string then
				result := not a_string.is_empty and then a_string.item (a_string.count) = a_character
			end
		ensure
			instance_free: class
		end

end
