note
	description: "Useful feature for handling the STRING_8 family"
	author: "obnosim"

expanded class
	STRING_8_UTIL

inherit

	STRING_UTIL

feature -- Constants

	Empty: STRING_8
			-- A static instance of an immutable empty string
		once ("process")
			create result.make_empty
		ensure
			empty: result.is_empty
			instance_free: class
		end

	Space: CHARACTER = ' '
			-- The space character

	Underscore: CHARACTER = '_'
			-- The underscore character

	New_line: CHARACTER = '%N'
			-- The new line character

	Null: CHARACTER = '%/000/'
			-- The null character

	Comma: CHARACTER = ','
			-- The comma character

	Open_parenthesis: CHARACTER = '('
			-- The opening parenthesis character

	Close_parenthesis: CHARACTER = ')'
			-- The closing parenthesis character

	Arobas: CHARACTER = '@'
			-- The arobas character, used to indicate a location

	Semicolon: CHARACTER = ';'
			-- The semicolon parenthesis character

	True_string: IMMUTABLE_STRING_8 = "True"
			-- `True' as a string

	False_string: IMMUTABLE_STRING_8 = "False"
			-- `False' as a string

	True_int_string: IMMUTABLE_STRING_8 = "1"
			-- `True' as an integer as a string

	False_int_string: IMMUTABLE_STRING_8 = "0"
			-- `False' as an integer as a string

feature -- Once constants

	Filesystem_separator: STRING_8
			-- Convenience access to the system-defined directory separator
		once ("process")
			create result.make_filled (operating_environment.Directory_separator, 1)
		ensure
			instance_free: class
			coherent: not result.is_empty
		end

feature -- Cleanup

	split_and_trim (a_string: READABLE_STRING_8; a_separator: CHARACTER_8): LIST [attached STRING_8]
			-- Splits `a_string' around `a_separator' and trims all parts
		do
			result := a_string.to_string_8.split (a_separator)
			trim (result)
		ensure
			instance_free: class
		end

	merge_lines (a_base: STRING_8; a_part: READABLE_STRING_8; a_separator: CHARACTER_8)
			-- Adds `a_part', stripped of external white space, to the end of `a_base', separated by `a_separator'.
			-- Useful to merge back a paragraph that was split into several lines while ignoring tabulations and such.
		local
			l_start, l_end: like {STRING_8}.lower
		do
			from
				l_start := a_part.lower
			until
				l_start > a_part.count or else not a_part [l_start].is_space
			loop
				l_start := l_start + 1
			end
			if l_start <= a_part.count then
				a_base.extend (a_separator)
				from
					l_end := a_part.count
				until
					l_end <= l_start or else not a_part [l_end].is_space
				loop
					l_end := l_end - 1
				end
				a_base.append (a_part.substring (l_start, l_end))
			end
		ensure
			instance_free: class
		end

	trimmed (a_raw: READABLE_STRING_8): STRING_8
			-- A version of `a_raw' without any surrounding white space.
			-- The result might be `a_raw' if it is already clean.
		local
			l_start, l_end: like {STRING_8}.lower
		do
			from
				l_start := a_raw.lower
			until
				l_start > a_raw.count or else not is_whitespace (a_raw [l_start])
			loop
				l_start := l_start + 1
			end
			if l_start > a_raw.count then
				result := empty
			else
				from
					l_end := a_raw.count
				until
					l_end <= l_start or else not is_whitespace (a_raw [l_start])
				loop
					l_end := l_end - 1
				end
				if l_start = a_raw.lower and l_end = a_raw.count then
					result := a_raw.to_string_8
				else
					result := a_raw.substring (l_start, l_end).to_string_8
				end
			end
			if a_raw = result then
				result := result.twin
			end
		ensure
			no_side_effect: a_raw.same_string (old a_raw.string) and result /= a_raw
			cleaned: (not result.is_empty) implies (not is_whitespace (result [result.lower]) and not is_whitespace (result [result.count]))
			instance_free: class
		end

	trimmed_copy (a_raw: READABLE_STRING_8): STRING_8
			-- A version of `a_raw' without any surrounding white space.
			-- Unline `trimmed', the result will always be a new object.
		do
			result := trimmed(a_raw)
			if result = a_raw then
				result := result.string
			end
		ensure
			never_input: result /= a_raw
			no_side_effect: a_raw.same_string (old a_raw.string)
			cleaned: (not result.is_empty) implies (not is_whitespace (result [result.lower]) and not is_whitespace (result [result.count]))
			instance_free: class
		end

	is_whitespace (a_char: CHARACTER_32): BOOLEAN
			-- Whether `a_char' is considered white space
		do
			result := not (a_char.is_alpha_numeric or a_char.is_punctuation)
		ensure
			instance_free: class
		end

feature -- Processing

	split (a_string: READABLE_STRING_8; a_separator: CHARACTER_8): LINKED_LIST [STRING_8]
			-- `a_string', split around `a_separator'
		do
			result := (create {WORD_SCANNER_8}.make (a_string, a_separator)).collected
		ensure
			instance_free: class
		end

	replace_all (a_string: STRING_8; a_source, a_target: CHARACTER_8)
			-- Replaces all occurrences of `a_source' within `a_string' with `a_target'
		local
			i: like {STRING_8}.Lower
		do
			from
				i := a_string.Lower
			until
				i > a_string.count
			loop
				if a_string [i] = a_source then
					a_string [i] := a_target
				end
				i := i + 1
			end
		ensure
			instance_free: class
		end

	substitute (a_string: READABLE_STRING_8; a_value: READABLE_STRING_8): STRING_8
			-- Replaces the first occurrence of `a_value' and `a_values'
		local
			l_key: STRING_8
			i: INTEGER
		do
			create result.make_from_string (a_string)
			l_key := "{1}"
			i := result.substring_index (l_key, result.Lower)
			if i > 0 then
				result.replace_substring (a_value, i, i + l_key.count - 1)
			else
				debug ("strings")
					token_not_found (a_string, l_key)
				end
			end
		ensure
			instance_free: class
		end

	substitute_many (a_string: READABLE_STRING_8; a_values: READABLE_INDEXABLE [READABLE_STRING_8]): STRING_8
			-- Replaces the first occurrence of `a_value' and `a_values'
		local
			l_key: STRING_8
			i: INTEGER
		do
			create result.make_from_string (a_string)
			across
				a_values as i_values
			loop
				l_key := "{"
				l_key.append_integer (i_values.cursor_index)
				l_key.extend ('}')
				i := result.substring_index (i_values.item, result.Lower)
				if i > 0 then
					result.replace_substring (i_values.item, i, i + l_key.count - 1)
				else
					debug ("strings")
						token_not_found (a_string, l_key)
					end
				end
			end
		ensure
			instance_free: class
		end

feature -- Status report

	string_starts_with (a_string: detachable READABLE_STRING_8; a_character: CHARACTER_8): BOOLEAN
			-- Whether `a_string' begins with `a_character'
			-- Similar to `a_string.starts_with' but with a single character
		do
			if attached a_string then
				result := not a_string.is_empty and then a_string.item (a_string.Lower) = a_character
			end
		ensure
			instance_free: class
		end

	string_ends_with (a_string: detachable READABLE_STRING_8; a_character: CHARACTER_8): BOOLEAN
			-- Whether `a_string' ends with `a_character'
			-- Similar to `a_string.ends_with' but with a single character
		do
			if attached a_string then
				result := not a_string.is_empty and then a_string.item (a_string.count) = a_character
			end
		ensure
			instance_free: class
		end

end
